#!/usr/bin/env bash
set -x

REPO_NAME=thirstydeveloper/workstation-config.git
HTTPS_REPO_REMOTE_URL=https://gitlab.com/${REPO_NAME}
SSH_REPO_REMOTE_URL=git@gitlab.com:${REPO_NAME}

function is-local() {
  if [[ "$(git remote get-url origin)" == "${HTTPS_REPO_REMOTE_URL}" ]]; then
    echo "true"
  elif [[ "$(git remote get-url origin)" == "${SSH_REPO_REMOTE_URL}" ]]; then
    echo "true"
  else
    echo ""
  fi
}

function bootstrap-darwin() {
  brew update
  brew install git ansible
}

function bootstrap-ubuntu() {
  sudo apt-get update
  sudo apt-get install software-properties-common
  sudo apt-add-repository --yes --update ppa:ansible/ansible
  sudo apt-get install git ansible --yes
}

while [ "$1" != "" ]; do
  case $1 in
    # Run using ansible-pull even if running from a local repository checkout
    -r | --force-remote)
      FORCE_REMOTE=1
      shift
      ;;
    -u | --update)
      SKIP_BOOTSTRAP=1
      shift
      ;;
    -v | --verbose)
      VERBOSITY_FLAG="${VERBOSITY_FLAG} -v"
      shift
      ;;
    -t | --tags)
      TAGS="--tags $2"
      shift 2;
      ;;
    -h | --help)
      usage
      exit 0
      ;;
    *)
      echo "Unsupported flag $1"
      usage
      exit 1
      ;;
  esac
done

if [ -f /etc/os-release ]; then
  echo "Detecting operating system from /etc/os-release"

  OS=$(awk -F= '/^NAME/{print $2}' /etc/os-release | sed -r 's/\s*"(.*)"/\1/g')
else
  OS=${OSTYPE}
fi

LOWERCASE_OS=$(echo $OS | tr '[:upper:]' '[:lower:]')

case "${LOWERCASE_OS}" in
  ubuntu)
    [ -z "${SKIP_BOOTSTRAP}" ] && bootstrap-ubuntu
    ;;

  darwin*)
    [ -z "${SKIP_BOOTSTRAP}" ] && bootstrap-darwin
    ;;

  *)
    echo "WARNING: unsupported operating system ${LOWERCASE_OS}. This will probably end badly."
    echo
    read -p "Press enter to continue anyways..."
    ;;
esac

if [[ "$(is-local)" ]] && [ -z "${FORCE_REMOTE}" ]; then
  ansible-playbook ${VERBOSITY_FLAG} ${TAGS} local.yml
else
  ansible-pull --accept-host-key --url ${HTTPS_REPO_REMOTE_URL}
fi
