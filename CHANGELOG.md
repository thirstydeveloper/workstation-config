# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v1.1.0] - 2024-10-17

### Added

- obsidian
- gh cli (osx)
- helm
- kubectl (osx)
- minikube (osx)

### Changed

- upgrade pyenv to 2.4.15
- upgrade nvm to v0.40.1
- upgrade vscode to 1.94.2
- upgrade fzf to 0.55.0
- upgrade packer on ubuntu to 1.11.2, osx to latest brew

### Fixed

- local install mode
- sdkman install on osx
- pipx install on osx
- fzf install on osx

### Removed

- evernote
- openjdk 8
- broken python packages
- broken setting of npm prefix (doesn't work with nvm)

## [v1.0.1] - 2019-11-17

### Fixed

* bootstrap installs git (required for ansible-pull)
* bootstrap uses https url
* bootstrap accepts host key on ansible-pull

## [v1.0.0] - 2019-11-17

Initial release
