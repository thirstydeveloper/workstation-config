Reconsider user-init role. Can bootstrap.sh do this? Check checksum on template files?

Unnest all vars

Refactor:
* AWS credentials less rigid (support more than two profiles)

Install:
* ag
* go, [goimports](https://godoc.org/golang.org/x/tools/cmd/goimports)

Research:
* ansible-vault for secrets
* molecule for testing

Integrate with dotfiles repo

Add bash alias for updating from repo

Add to pipeline tests for:
* applying to ubuntu docker/vagrant
* something to simulate OSX?

Automate release process
