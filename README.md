# Usage

`curl -s https://gitlab.com/thirstydeveloper/workstation-config/raw/master/bootstrap.sh | bash`

If this is the first execuction, the script will pause for you to fill in:

* `~/etc/ansible/workstation.yml`
* `~/etc/ansible/secrets.yml`

## Running Locally

`./bootstrap.sh` can be run from a local clone to test changes.

# Forking

You'll want to update `REPO_NAME` and `REMOTE_URL` in [bootstrap.sh](./bootstrap.sh)

# Pre-requisites

* sudo
