export PYENV_ROOT={{ install_path }}

pathmunge "${PYENV_ROOT}/bin"

if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
