bashrc-debug() {
  local msg=$1

  [[ -n "${BASHRC_DEBUG}" ]] && echo $msg
}

pathmunge() {
  if ! echo $PATH | egrep -q "(^|:)$1($|:)"; then
    if [ "$2" = "after" ]; then
      PATH=$PATH:$1
    else
      PATH=$1:$PATH
    fi
  fi
}

# pathmunge doesn't seem to work for pre-pending on OSX for some reason...
export PATH=${HOME}/bin:${HOME}/.local/bin:${PATH}

alias reload='source ${HOME}/.bashrc'
