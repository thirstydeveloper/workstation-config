# Determine OS

if [ -f /etc/os-release ]; then
  bashrc-debug "Detecting operating system from /etc/os-release"

  OS=$(awk -F= '/^NAME/{print $2}' /etc/os-release | sed -r 's/\s*"(.*)"/\1/g')
else
  OS=${OSTYPE}
fi

OS=$(echo $OS | tr '[:upper:]' '[:lower:]')
